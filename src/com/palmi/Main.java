package com.palmi;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) throws IOException {
        Path mainDirPath;

        do {
            mainDirPath = Utility.getPath(Utility.userPath());
        } while (mainDirPath==null);

        Stream<Path> mainDirStream = Files.list(mainDirPath);
        Map<String, Long> filesFoldersCount = Utility.getFilesFoldersCount(mainDirStream, Utility.checkHidden());
        Utility.printFilesFoldersCount(filesFoldersCount);
    }
}
