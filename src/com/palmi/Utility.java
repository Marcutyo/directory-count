package com.palmi;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class Utility {
    private Utility() {}
    private static final Scanner INPUT = new Scanner(System.in);

    public static Map<String, Long> getFilesFoldersCount(Stream<Path> pathStream, boolean checkHidden) throws IOException {
        return getFileStream(pathStream, checkHidden)
            .map(Utility::getExtension)
            .collect(Collectors.groupingBy(fileExt -> fileExt, Collectors.counting()))
        ;
    }

    public static Stream<File> getFileStream(Stream<Path> pathStream, boolean checkHidden) throws IOException {
        return getPathStream(pathStream, checkHidden)
                .flatMap(file -> {
                    try {
                        return file.isDirectory() ?
                                Stream.concat(Stream.of(file), getFileStream(Files.list(Paths.get(file.getAbsolutePath())), checkHidden)) :
                                Stream.of(file);
                    } catch (IOException e) {
                        System.out.println("Errore I/O Exception: impossibile trovare la/le risorsa/e specificata/e.");
                        return null;
                    }
                });
    }

    public static String userPath() {
        System.out.println("Inserisci un percorso valido ed esistente (C:\\cartella-1\\cartella-2\\...)");
        return INPUT.nextLine();
    }

    public static Path getPath (String path) {
        try {
            return  Paths.get(path).toFile().canRead() && Paths.get(path).toFile().isDirectory() ?
                    Paths.get(path) :
                    getPath(userPath());
        } catch (InvalidPathException e) {
            System.out.println("Errore: percorso inserito non valido.");
            return getPath(userPath());
        }
    }

    public static Stream<File> getPathStream(Stream<Path> pathStream, boolean checkHidden){
        return checkHidden ?
                pathStream.map(Path::toFile) :
                pathStream.map(Path::toFile)
                .filter(file -> !file.isHidden());
    }

    public static String getExtension(File file){
        if (file.isDirectory()) return "cartelle";
        else return file.getName().contains(".") ?
                    file.getName().substring(file.getName().lastIndexOf(".") + 1) :
                    "senza estensione";
    }

    public static boolean checkHidden() {
        System.out.println("Vuoi controllare file e/o cartelle nascosti/e? - [Y] Sì [Altro] No");
        boolean checkHidden = INPUT.next().equalsIgnoreCase("y");
        System.out.println(checkHidden ?
                "Verranno conteggiati/e anche file e/o cartelle nascosti/e." :
                "File e/o cartelle nascosti/e non verranno conteggiati/e.");
        return checkHidden;
    }

    public static void printFilesFoldersCount(Map<String, Long> filesFoldersCount) {
        System.out.println("\nElenco file e/o cartelle e conteggio:");
        filesFoldersCount
                .forEach((fileExt, count) -> System.out.println("• " + fileExt + ": " + count));
    }

}

